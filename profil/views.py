from django.shortcuts import render

# Create your views here.
def profile(request):
    return render(request, 'profile.html')

def arnold(request):
    return render(request, 'arnold.html')

def renatta(request):
    return render(request, 'renatta.html')

def juna(request):
    return render(request, 'juna.html')

def farahquin(request):
    return render(request, 'farahquin.html')

def index(request):
    return render(request, 'index.html')

