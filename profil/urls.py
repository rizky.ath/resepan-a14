from django.urls import path

from . import views

app_name = 'profil'

urlpatterns = [
	path('', views.profile, name='profile'),
    path('arnold/', views.arnold, name='arnold'),
    path('renatta/', views.renatta, name='renatta'),
    path('juna/', views.juna, name='juna'),
    path('farahquin/', views.farahquin, name='farahquin'),
    path('', views.index, name='index'),
]
