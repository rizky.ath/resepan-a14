from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from django.apps import apps
from .models import Ulasan
from .forms import FormUlasan
from .views import ulasan, listReview, remove
from .apps import ReviewConfig



# Create your tests here.
class ModelTest(TestCase):
    def setUp(self):
        self.ulasan = Ulasan.objects.create(
            nama_pengulas="alipeh", isi_ulasan="aplikasinya bagus")

    def test_instance_created(self):
        self.assertEqual(Ulasan.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.ulasan), "alipeh")

class FormTest(TestCase):
    def test_form_is_valid(self):
        form_ulasan = FormUlasan(data={
            "nama_pengulas": "alipeh",
            "isi_ulasan" : "aplikasinya bagus"
        })
        self.assertTrue(form_ulasan.is_valid())

    def test_form_invalid(self):
        form_ulasan = FormUlasan(data={})
        self.assertFalse(form_ulasan.is_valid())

class UrlsTest(TestCase):
    def setUp(self):
        self.ulasan = Ulasan.objects.create(
            nama_pengulas="alipeh", isi_ulasan="aplikasinya bagus")
        self.listReview = reverse("review:listReview")
        self.remove = reverse("review:remove", args=[self.ulasan.pk])
        self.review = reverse("review:ulasan")

    def test_listReview_use_right_function(self):
        found = resolve(self.listReview)
        self.assertEqual(found.func, listReview)

    def test_ulasan_use_right_function(self):
        found = resolve(self.review)
        self.assertEqual(found.func, ulasan)

    def test_remove_use_right_function(self):
        found = resolve(self.remove)
        self.assertEqual(found.func, remove)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.listReview = reverse("review:listReview")
        self.ulasan = reverse("review:ulasan")

    def test_GET_listReview(self):
        response = self.client.get(self.listReview)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'stuff.html')

    def test_GET_ulasan(self):
        response = self.client.get(self.ulasan)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'review.html')

    def test_POST_ulasan(self):
        response = self.client.post(self.ulasan,
                                    {
                                        'nama_pengulas': 'alipeh', 'isi_ulasan':'aplikasinya bagus'
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_ulasan_invalid(self):
        response = self.client.post(self.ulasan,
                                    {
                                        'nama_kegiatan': '', 'isi_ulasan':''
                                    }, follow=True)
        self.assertTemplateUsed(response, 'review.html')

    def test_GET_delete(self):
        ulasan = Ulasan(nama_pengulas="alipeh", isi_ulasan="aplikasinya bagus")
        ulasan.save()
        response = self.client.get(reverse('review:remove', args=[ulasan.pk]))
        self.assertEqual(Ulasan.objects.count(), 0)
        self.assertEqual(response.status_code, 302)

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(ReviewConfig.name, 'review')
        self.assertEqual(apps.get_app_config('review').name, 'review')

    


