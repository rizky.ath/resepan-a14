from django import forms

class FormUlasan(forms.Form):
    nama_pengulas = forms.CharField(
        max_length = 20,
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Masukkan nama Anda'
            }
        )
    )

    isi_ulasan = forms.CharField(
        max_length = 200,
        widget = forms.Textarea(
            attrs = {
                'class':'form-control',
                'placeholder':'Masukkan ulasan Anda'
            }
        )
    )