from django.urls import path

from . import views

app_name = 'review'

urlpatterns = [
        path('ulasan', views.ulasan, name='ulasan'),
        path('listReview', views.listReview, name='listReview'),
        path('result/<int:id>',views.remove, name = 'remove'),



]
