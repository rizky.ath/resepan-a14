from django.db import models
# Create your models here.

class Ulasan(models.Model):
    nama_pengulas = models.CharField(max_length=20, blank=False)
    isi_ulasan = models.TextField(max_length=130, blank=False)
    tanggal_dibuat = models.DateTimeField(auto_now=True)


    def __str__(self):
        return self.nama_pengulas
