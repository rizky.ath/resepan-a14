from django.shortcuts import render, redirect
from django.http import HttpResponse
from review.models import Ulasan
from review import forms
from review.forms import FormUlasan

# Create your views here.
def ulasan(request):
    form_ulasan = forms.FormUlasan(request.POST or None)
    if request.method == 'POST':
        if form_ulasan.is_valid():
            data = form_ulasan.cleaned_data
            Ulasan_input = Ulasan()
            Ulasan_input.nama_pengulas = data['nama_pengulas']
            Ulasan_input.isi_ulasan = data['isi_ulasan']
            Ulasan_input.save()
            print(data)
            return redirect('review:listReview')
        else:
            current_data = Ulasan.objects.all().values()
            return render(request, 'review.html',{'form':form_ulasan, 'status':'failed','data':current_data})
    else:
        current_data = Ulasan.objects.all()
        return render(request, 'review.html',{'form':form_ulasan,'data':current_data})

def listReview (request):
    ulasan = Ulasan.objects.all().values()
    return render(request, 'stuff.html', {'ulasan': ulasan})

def remove(request, id):
    Ulasan.objects.filter(id=id).delete()
    return redirect('review:listReview')