from django.contrib import admin

# Register your models here.
from .models import Resep

class ResepAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)

admin.site.register(Resep, ResepAdmin)
