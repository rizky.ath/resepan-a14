from django.urls import path

from add.views import add, detail
from add import views

app_name = 'add'

urlpatterns = [
    path('', add, name='add'),
    path('detail/<int:detail_id>/', detail, name='detail')

]
