from django.shortcuts import render, redirect

from .forms import ResepModelForm
from add.models import Resep

def add(request):
    template_name = 'add/add.html'
    if request.method == 'GET':
        resepform = ResepModelForm(request.GET or None)
    elif request.method == 'POST':
        resepform = ResepModelForm(request.POST, request.FILES)
        if resepform.is_valid():
            resep = resepform.save()
            return redirect('search:idx')
    
    return render(request, template_name, {
        'resepform': resepform,
    })

def detail(request, detail_id):
    resep_detail = Resep.objects.get(id=detail_id)
    #ulasan = Ulasan.objects.all()
    #form = FormUlasan()
    context = {
        'resep_detail':resep_detail
        #'ulasan': ulasan,
        #'form': form
        }
    return render(request, 'resep-details.html', context)