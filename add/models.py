from django.db import models


class Resep(models.Model):
    foto = models.ImageField(upload_to='images')
    nama = models.CharField(max_length=255)
    deskripsi = models.CharField(max_length=255)
    porsi = models.CharField(max_length=255)
    durasi = models.CharField(max_length=255)
    bahan = models.TextField()
    langkah = models.TextField()

    def __str__(self):
        return self.nama
