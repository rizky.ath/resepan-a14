from django import forms
from django.forms import modelformset_factory

from .models import Resep

class ResepModelForm(forms.ModelForm):

    class Meta:
        model = Resep
        fields = (
            'foto',
            'nama',
            'deskripsi',
            'porsi',
            'durasi',
            'bahan',
            'langkah', 
        )
        labels = {
            'nama': 'Nama Masakan',
            'deskripsi': 'Deskripsi',
            'porsi': 'Porsi',
            'durasi':'Lama Memasak',
        }
        widgets = {
            'nama': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Fried Rice'
                }
            ),
            'deskripsi': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Description'
                }
            ),
            'porsi': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': '3 people'
                }
            ),
            'durasi': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': '1 hour 30 minutes'
                }
            ),
            'bahan': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Ingredients',
                'white-space': 'pre-wrap',
                }
            ),
            'langkah': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Instructions',
                'white-space': 'pre-wrap',
                }
            ),
        }