from django.urls import path

from .views import idx, result_by_max_keyword

app_name = 'search'

urlpatterns = [
	path('', idx, name='idx'),
	path('<str:pk>', result_by_max_keyword, name='res_max')
]
